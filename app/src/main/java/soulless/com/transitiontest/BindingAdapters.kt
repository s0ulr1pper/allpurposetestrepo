package soulless.com.transitiontest

import android.databinding.BindingAdapter
import android.support.annotation.DrawableRes
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.DynamicDrawableSpan
import android.text.style.ImageSpan
import android.widget.TextView

/**
 *
 * @author novikov
 *         Date: 11.10.2018
 */
@BindingAdapter("endImage")
fun bindTextImage(textView: TextView, @DrawableRes endDrawableRes: Int) {
    val spannable = SpannableStringBuilder(textView.text)
        .append(
            " ",
            ImageSpan(textView.context, endDrawableRes, DynamicDrawableSpan.ALIGN_BASELINE),
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

    textView.text = spannable
}