package soulless.com.transitiontest.util

import android.util.Log

const val isLogEnabled = true

fun logv(input: Any?, throwable: Throwable? = null, tag: String = "XXX") {
    if (isLogEnabled) {
        Log.v(tag, input.toString(), throwable)
    }
}

fun logi(input: Any?, throwable: Throwable? = null, tag: String = "XXX") {
    if (isLogEnabled) {
        Log.i(tag, input.toString(), throwable)
    }
}

fun logd(input: Any?, throwable: Throwable? = null, tag: String = "XXX") {
    if (isLogEnabled) {
        Log.d(tag, input.toString(), throwable)
    }
}

fun logw(input: Any?, throwable: Throwable? = null, tag: String = "XXX") {
    if (isLogEnabled) {
        Log.w(tag, input.toString(), throwable)
    }
}

fun loge(throwable: Throwable? = null, tag: String = "XXX") {
    if (isLogEnabled) {
        Log.e(tag, throwable?.toString(), throwable)
    }
}