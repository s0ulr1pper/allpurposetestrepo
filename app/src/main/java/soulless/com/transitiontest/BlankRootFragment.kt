package soulless.com.transitiontest

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class BlankRootFragment : Fragment() {

    var pos = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_fragment_placeholder, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(R.id.left_button).setOnClickListener {
            test(++pos)
        }

        view.findViewById<Button>(R.id.right_button).setOnClickListener {
//            childFragmentManager?.popBackStack(
//                "1${BlankFragment::class.java.canonicalName}",
//                POP_BACK_STACK_INCLUSIVE
//            )
            fragmentManager?.popBackStack()
        }

//        Handler().postDelayed({
//            Log.d("XXX", "popped")
//            fragmentManager?.popBackStack()
//        }, 5000)
    }

    private fun test(pos: Int) {
        childFragmentManager.beginTransaction().also {
            it.setCustomAnimations(
                R.anim.slide_in_top,
                R.anim.slide_out_bottom,
                R.anim.slide_in_top,
                R.anim.slide_out_bottom
            )
            val tag = pos.toString() + BlankFragment::class.java.canonicalName
            Log.d("XXX", "adding fragment to $tag")
            it.addToBackStack(tag)
        }.add(R.id.fragment_placeholder, BlankFragment()).commit()
    }


}
