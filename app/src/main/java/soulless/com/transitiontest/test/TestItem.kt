package soulless.com.transitiontest.test

import soulless.com.transitiontest.R
import soulless.com.transitiontest.databinding.ItemTestBinding
import soulless.com.transitiontest.list.BindableItem
import soulless.com.transitiontest.list.BindingViewHolder

/**
 *
 * @author novikov
 *         Date: 12.10.2018
 */
class TestItem(
    private val viewModel: TestViewModel
) : BindableItem<ItemTestBinding>() {

    override fun getSpanSize(defaultSpanSize: Int) = defaultSpanSize

    override fun bind(viewHolder: BindingViewHolder<ItemTestBinding>) {
        viewHolder.binding.itemTestRoot.setBackgroundColor(viewModel.color)
        viewHolder.binding.testId.text = viewModel.order.toString()
    }

    override fun getLayout() = R.layout.item_test
}