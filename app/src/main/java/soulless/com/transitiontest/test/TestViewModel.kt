package soulless.com.transitiontest.test

import android.support.annotation.ColorInt

/**
 *
 * @author novikov
 *         Date: 12.10.2018
 */
class TestViewModel(
    val order: Int,
    @ColorInt val color: Int
)