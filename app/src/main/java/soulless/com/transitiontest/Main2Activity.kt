package soulless.com.transitiontest

import android.databinding.DataBindingUtil
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import soulless.com.transitiontest.databinding.ActivityMain2Binding
import soulless.com.transitiontest.list.GroupAdapter
import soulless.com.transitiontest.list.Section
import soulless.com.transitiontest.test.TestItem
import soulless.com.transitiontest.test.TestPlaceholderItem
import soulless.com.transitiontest.test.TestViewModel
import java.util.*


class Main2Activity : AppCompatActivity() {

    private val randomColor: Int
        get() {
            val rnd = Random()
            return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
        }

    private val section = Section().also {
        it.setPlaceholder(TestPlaceholderItem())
    }

    private val adapter = GroupAdapter(1).also {
        it.addGroup(section)
    }

    private val content = List(100) {
        TestItem(TestViewModel(it, randomColor))
    }

    private val binding by lazy(LazyThreadSafetyMode.NONE) {
        DataBindingUtil.setContentView<ActivityMain2Binding>(this, R.layout.activity_main2)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.testRecycler.also {
            it.adapter = adapter
            it.layoutManager = GridLayoutManager(
                it.context,
                adapter.defaultSpanSize,
                LinearLayoutManager.VERTICAL,
                false
            ).apply {
                spanSizeLookup = adapter.spanSizeLookup
            }
        }

        binding.leftButton.setOnClickListener {
            section.update(content)
        }

        binding.rightButton.setOnClickListener {
            section.update(emptyList())
        }
    }
}