package soulless.com.transitiontest.list

/**
 * The "collapsed"/header item of an expanded group.  Some part (or all) of it is a "toggle" to
 * expand the group.
 *
 * Collapsed:
 * - This
 *
 * Expanded:
 * - This
 * - Child
 * - Child
 * - etc
 *
 */
interface ExpandableItem : Group {
    fun setExpandableGroup(onToggleListener: ExpandableGroup)
}
