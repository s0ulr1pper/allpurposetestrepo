package soulless.com.transitiontest.list

import android.support.annotation.LayoutRes
import android.support.v7.util.DiffUtil
import android.support.v7.util.ListUpdateCallback
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import soulless.com.transitiontest.util.logv
import kotlin.system.measureTimeMillis

/**
 *
 * @author novikov
 *         Date: 22.11.2017
 */
class GroupAdapter(
    val defaultSpanSize: Int,
    private val prefetchItems: Int = 0
) : RecyclerView.Adapter<ViewHolder>(), GroupDataObserver {

    private lateinit var lastItemLookup: Item<ViewHolder>

    private val diffUtilCallbacks = object : AsyncDiffUtil.Callback {

        override fun onDispatchResult(newGroups: Collection<Group>) {
            groups.clear()
            groups.addAll(newGroups)
        }

        override fun onInserted(position: Int, count: Int) {
            notifyItemRangeInserted(position, count)
        }

        override fun onRemoved(position: Int, count: Int) {
            notifyItemRangeRemoved(position, count)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            notifyItemMoved(fromPosition, toPosition)
        }

        override fun onChanged(position: Int, count: Int, payload: Any?) {
            notifyItemRangeChanged(position, count, payload)
        }
    }

    private val asyncDiffUtil = AsyncDiffUtil(diffUtilCallbacks)

    /**
     * Replace all existing body content and dispatch fine-grained change notifications to the
     * parent using DiffUtil.
     *
     * Item comparisons are made using:
     * - Item.isSameAs(Item otherItem) (are items the same?)
     * - Item.equals() (are contents the same?)
     *
     *
     * If you don't customize getId() or isSameAs() and equals(), the default implementations will return false,
     * meaning your Group will consider every update a complete change of everything.
     *
     * @param newGroups The new content of the adapter
     * @param forceUpdate forces update whole section content, ignore DiffUtil
     */
    fun update(newGroups: Collection<Group>, forceUpdate: Boolean = false) {
        val oldGroups = ArrayList(groups)
        val oldBodyItemCount = getItemCount(oldGroups)
        val newBodyItemCount = getItemCount(newGroups)

        for (group in groups) {
            group.unregisterGroupDataObserver(this)
        }

        groups.clear()
        groups.addAll(newGroups)

        for (group in newGroups) {
            group.registerGroupDataObserver(this)
        }

        if (forceUpdate) {
            when {
                oldBodyItemCount == newBodyItemCount -> notifyItemRangeChanged(0, oldBodyItemCount)
                oldBodyItemCount < newBodyItemCount -> {
                    notifyItemRangeChanged(0, oldBodyItemCount)
                    notifyItemRangeInserted(oldBodyItemCount, newBodyItemCount - oldBodyItemCount)
                }
                oldBodyItemCount > newBodyItemCount -> {
                    notifyItemRangeChanged(0, newBodyItemCount)
                    notifyItemRangeRemoved(newBodyItemCount, oldBodyItemCount - newBodyItemCount)
                }
            }
            return
        }

        val diffResult = DiffUtil.calculateDiff(
            DiffCallback(oldBodyItemCount, newBodyItemCount, oldGroups, newGroups)
        )

        diffResult.dispatchUpdatesTo(listUpdateCallback)

    }

    private val listUpdateCallback: ListUpdateCallback = object : ListUpdateCallback {
        override fun onInserted(position: Int, count: Int) {
            notifyItemRangeInserted(position, count)
        }

        override fun onRemoved(position: Int, count: Int) {
            notifyItemRangeRemoved(position, count)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            notifyItemMoved(fromPosition, toPosition)
        }

        override fun onChanged(position: Int, count: Int, payload: Any?) {
            notifyItemRangeChanged(position, count, payload)
        }
    }

    private val groups: MutableList<Group> = ArrayList()

    val spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {

        override fun getSpanSize(position: Int): Int {
            return getItem(position).getSpanSize(defaultSpanSize)
        }
    }

    fun updateAsync(newGroups: List<Group>) {
        val oldGroups = ArrayList(groups)
        val oldBodyItemCount = getItemCount(oldGroups)
        val newBodyItemCount = getItemCount(newGroups)

        asyncDiffUtil.calculateDiff(
            newGroups,
            DiffCallback(oldBodyItemCount, newBodyItemCount, oldGroups, newGroups)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int, payloads: List<Any>) {
        val contentItem = getItem(position)
        contentItem.bindViewHolder(holder, position, payloads)
        // only prepare positive number of items
        if (prefetchItems <= 0) {
            return
        }
        (position + 1..position + prefetchItems).forEach {
            // in case of last item
            if (it < itemCount) {
                getItem(it).prepareItem(holder)
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // do nothing
    }

    override fun onCreateViewHolder(parent: ViewGroup, layoutId: Int): ViewHolder {
        var holder: ViewHolder? = null
        val elapsed = measureTimeMillis {
            val inflater = LayoutInflater.from(parent.context)
            val item = getItemForViewType(layoutId)
            val itemView = inflater.inflate(layoutId, parent, false)
            holder = item.createViewHolder(itemView)
        }
        logv("create view holder took $elapsed", tag = "LT.GroupAdapter")
        return holder ?: throw RuntimeException()
    }

    override fun onViewRecycled(holder: ViewHolder) {
        holder.item.recycle(holder)
    }

    override fun getItemCount(): Int {
        return (0 until groups.size)
            .sumBy { groups[it].getItemCount() }
    }

    fun getItem(position: Int): Item<ViewHolder> {
        var count = 0
        (0 until groups.size)
            .forEach {
                val group = groups[it]
                if (position < count + group.getItemCount()) {
                    return group.getItem(position - count)
                } else {
                    count += group.getItemCount()
                }
            }
        throw IndexOutOfBoundsException(
            "Requested position $position in group adapter " +
                    "but there are only $count items"
        )
    }

    override fun getItemViewType(position: Int): Int {
        lastItemLookup = getItem(position)
        return lastItemLookup.getLayout()
    }

    override fun getItemId(position: Int): Long {
        return getItem(position).id
    }

    fun removeAll(groups: Collection<Group>) {
        groups.forEach(::remove)
    }

    fun remove(group: Group) {
        val position = groups.indexOf(group)
        remove(position, group)
    }

    fun removeGroup(index: Int) {
        val group = getGroup(index)
        remove(index, group)
    }

    private fun remove(position: Int, group: Group) {
        val itemCountBeforeGroup = getItemCountBeforeGroup(position)
        group.unregisterGroupDataObserver(this)
        groups.removeAt(position)
        notifyItemRangeRemoved(itemCountBeforeGroup, group.getItemCount())
    }

    private fun getItemCountBeforeGroup(groupIndex: Int): Int =
        (0 until groupIndex)
            .sumBy {
                groups[it].getItemCount()
            }

    private fun getGroup(position: Int): Group {
        var previous = 0
        for (i in 0 until groups.size) {
            val group = groups[i]
            if (position - previous <= group.getItemCount()) {
                return group
            }
            previous += group.getItemCount()
        }
        throw IndexOutOfBoundsException(
            "Requested position " + position + "in group adapter " +
                    "but there are only " + previous + " items"
        )
    }

    fun getGroup(contentItem: Item<ViewHolder>): Group {
        (0 until groups.size)
            .filter { groups[it].getItemPosition(contentItem) >= 0 }
            .forEach { return groups[it] }
        throw IndexOutOfBoundsException("Item is not present in adapter or in any group")
    }

    fun getAdapterPosition(contentItem: Item<*>): Int {
        var count = 0
        (0 until groups.size).forEach {
            val index = groups[it].getItemPosition(contentItem)
            if (index >= 0) return index + count
            count += groups[it].getItemCount()
        }
        return -1
    }

    /**
     * The position in the flat list of individual items at which the group starts
     *
     * @param group
     * @return
     */
    fun getAdapterPosition(group: Group): Int {
        val index = groups.indexOf(group)
        if (index == RecyclerView.NO_POSITION) return RecyclerView.NO_POSITION
        return (0 until index).sumBy { groups[it].getItemCount() }
    }

    fun addGroup(index: Int, group: Group) {
        if (groups.size < index) {
            addGroup(group)
            return
        }
        group.registerGroupDataObserver(this)
        groups.add(index, group)
        val itemCountBeforeGroup = getItemCountBeforeGroup(index)
        notifyItemRangeInserted(itemCountBeforeGroup, group.getItemCount())
    }

    fun addGroup(group: Group) {
        val itemCountBeforeGroup = itemCount
        groups.add(group)
        group.registerGroupDataObserver(this)
        notifyItemRangeInserted(itemCountBeforeGroup, group.getItemCount())
    }

    fun addAll(groups: Collection<Group>) {
        val itemCountBeforeGroup = itemCount
        val additionalSize = groups.sumBy {
            it.registerGroupDataObserver(this)
            it.getItemCount()
        }
        this.groups.addAll(groups)
        notifyItemRangeInserted(itemCountBeforeGroup, additionalSize)
    }

    fun clear() {
        groups.forEach {
            it.unregisterGroupDataObserver(this)
        }
        groups.clear()
        notifyDataSetChanged()
    }

    override fun onChanged(group: Group) {
        notifyItemRangeChanged(getAdapterPosition(group), group.getItemCount())
    }

    override fun onItemInserted(group: Group, position: Int) {
        notifyItemInserted(getAdapterPosition(group) + position)
    }

    override fun onItemChanged(group: Group, position: Int) {
        notifyItemChanged(getAdapterPosition(group) + position)
    }

    override fun onItemChanged(group: Group, position: Int, payload: Any?) {
        notifyItemChanged(getAdapterPosition(group) + position, payload)
    }

    override fun onItemRemoved(group: Group, position: Int) {
        notifyItemRemoved(getAdapterPosition(group) + position)
    }

    override fun onItemRangeChanged(group: Group, positionStart: Int, itemCount: Int) {
        notifyItemRangeChanged(getAdapterPosition(group) + positionStart, itemCount)
    }

    override fun onItemRangeChanged(
        group: Group,
        positionStart: Int,
        itemCount: Int,
        payload: Any?
    ) {
        notifyItemRangeChanged(getAdapterPosition(group) + positionStart, itemCount, payload)
    }

    override fun onItemRangeInserted(group: Group, positionStart: Int, itemCount: Int) {
        notifyItemRangeInserted(getAdapterPosition(group) + positionStart, itemCount)
    }

    override fun onItemRangeRemoved(group: Group, positionStart: Int, itemCount: Int) {
        notifyItemRangeRemoved(getAdapterPosition(group) + positionStart, itemCount)
    }

    override fun onItemMoved(group: Group, fromPosition: Int, toPosition: Int) {
        val groupAdapterPosition = getAdapterPosition(group)
        notifyItemMoved(groupAdapterPosition + fromPosition, groupAdapterPosition + toPosition)
    }

    private fun getItemForViewType(@LayoutRes layoutResId: Int): Item<ViewHolder> {
        if (lastItemLookup.getLayout() == layoutResId) {
            // We expect this to be a hit 100% of the time
            return lastItemLookup
        }

        // To be extra safe in case RecyclerView implementation details change...
        for (i in 0 until itemCount) {
            val item = getItem(i)
            if (item.getLayout() == layoutResId) {
                return item
            }
        }

        throw IllegalStateException("Could not find model for view type: $layoutResId")
    }
}

private fun getItemCount(groups: Collection<Group>): Int {
    var count = 0
    for (group in groups) {
        count += group.getItemCount()
    }
    return count
}

private fun getItem(groups: Collection<Group>, position: Int): Item<*> {
    var count = 0
    for (group in groups) {
        if (position < count + group.getItemCount()) {
            return group.getItem(position - count)
        } else {
            count += group.getItemCount()
        }
    }
    throw IndexOutOfBoundsException(
        "Requested position " + position + "in group adapter " +
                "but there are only " + count + " items"
    )
}

private class DiffCallback internal constructor(
    private val oldBodyItemCount: Int,
    private val newBodyItemCount: Int,
    private val oldGroups: Collection<Group>,
    private val newGroups: Collection<Group>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldBodyItemCount
    }

    override fun getNewListSize(): Int {
        return newBodyItemCount
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = getItem(oldGroups, oldItemPosition)
        val newItem = getItem(newGroups, newItemPosition)
        return newItem.isSameAs(oldItem)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = getItem(oldGroups, oldItemPosition)
        val newItem = getItem(newGroups, newItemPosition)
        return newItem == oldItem
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val oldItem = getItem(oldGroups, oldItemPosition)
        val newItem = getItem(newGroups, newItemPosition)
        return oldItem.getChangePayload(newItem)
    }
}