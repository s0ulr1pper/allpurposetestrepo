package soulless.com.transitiontest.list

import soulless.com.transitiontest.list.Group
import soulless.com.transitiontest.list.Item
import soulless.com.transitiontest.list.Section
import soulless.com.transitiontest.list.ViewHolder

/**
 *
 * @author novikov
 *         Date: 12.12.2017
 */
class HiddenSection(
    header: Group? = null,
    list: List<Item<out ViewHolder>>,
    private var isExpanded: Boolean = false
) : Section(header, list) {

    override fun getGroupCount() = if (isExpanded) super.getGroupCount() else 0

    fun onToggle() {
        val oldSize = getItemCount()
        isExpanded = !isExpanded
        val newSize = getItemCount()
        if (oldSize > newSize) {
            notifyItemRangeRemoved(newSize, oldSize - newSize)
        } else {
            notifyItemRangeInserted(oldSize, newSize - oldSize)
        }
    }
}