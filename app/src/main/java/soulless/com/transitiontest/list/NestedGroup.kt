package soulless.com.transitiontest.list

import android.support.annotation.CallSuper
import java.util.*

/**
 *
 * @author novikov
 *         Date: 12.12.2017
 */
abstract class NestedGroup : Group, GroupDataObserver {

    private val observable = GroupDataObservable()

    abstract fun getGroup(position: Int): Group

    abstract fun getGroupCount(): Int

    final override fun getItemCount(): Int {
        return (0 until getGroupCount())
            .sumBy { getGroup(it).getItemCount() }
    }

    protected fun getItemCount(groups: Collection<Group>): Int = groups.sumBy { it.getItemCount() }

    /**
     * Gets the position of an
     *
     * @param item
     * @return
     */
    override fun getItemPosition(item: Group): Int {
        var previousPosition = 0

        for (i in 0 until getGroupCount()) {
            val group = getGroup(i)
            val position = group.getItemPosition(item)
            if (position >= 0) {
                return position + previousPosition
            }
            previousPosition += group.getItemCount()
        }

        return -1
    }

    protected fun getItemCountBeforeGroup(group: Group): Int {
        val groupIndex = getItemPosition(group)
        return getItemCountBeforeGroup(groupIndex)
    }

    private fun getItemCountBeforeGroup(groupIndex: Int): Int {
        return (0 until groupIndex)
            .sumBy { getGroup(it).getItemCount() }
    }

    final override fun getItem(position: Int): Item<ViewHolder> {
        var previousPosition = 0

        for (i in 0 until getGroupCount()) {
            val group = getGroup(i)
            val size = group.getItemCount()
            if (size + previousPosition > position) {
                return group.getItem(position - previousPosition)
            }
            previousPosition += size
        }

        throw IndexOutOfBoundsException(
            "Wanted item at " + position + " but there are only "
                    + getItemCount() + " items"
        )
    }

    override fun registerGroupDataObserver(groupDataObserver: GroupDataObserver) {
        observable.registerObserver(groupDataObserver)
    }

    override fun unregisterGroupDataObserver(groupDataObserver: GroupDataObserver) {
        observable.unregisterObserver(groupDataObserver)
    }

    @CallSuper
    open fun add(group: Group) {
        group.registerGroupDataObserver(this)
    }

    @CallSuper
    open fun addAll(groups: Collection<Group>) {
        for (group in groups) {
            group.registerGroupDataObserver(this)
        }
    }

    @CallSuper
    open fun add(position: Int, group: Group) {
        group.registerGroupDataObserver(this)
    }

    @CallSuper
    open fun addAll(position: Int, groups: Collection<Group>) {
        for (group in groups) {
            group.registerGroupDataObserver(this)
        }
    }

    @CallSuper
    open fun remove(group: Group) {
        group.unregisterGroupDataObserver(this)
    }

    @CallSuper
    open fun removeAll(groups: Collection<Group>) {
        for (group in groups) {
            group.unregisterGroupDataObserver(this)
        }
    }

    /**
     * Every item in the group still exists but the data in each has changed (e.g. should rebind).
     *
     * @param group
     */
    @CallSuper
    override fun onChanged(group: Group) {
        observable.onItemRangeChanged(this, getItemCountBeforeGroup(group), group.getItemCount())
    }

    @CallSuper
    override fun onItemInserted(group: Group, position: Int) {
        observable.onItemInserted(this, getItemCountBeforeGroup(group) + position)
    }

    @CallSuper
    override fun onItemChanged(group: Group, position: Int) {
        observable.onItemChanged(this, getItemCountBeforeGroup(group) + position)
    }

    @CallSuper
    override fun onItemChanged(group: Group, position: Int, payload: Any?) {
        observable.onItemChanged(this, getItemCountBeforeGroup(group) + position, payload)
    }

    @CallSuper
    override fun onItemRemoved(group: Group, position: Int) {
        observable.onItemRemoved(this, getItemCountBeforeGroup(group) + position)
    }

    @CallSuper
    override fun onItemRangeChanged(group: Group, positionStart: Int, itemCount: Int) {
        observable.onItemRangeChanged(
            this,
            getItemCountBeforeGroup(group) + positionStart,
            itemCount
        )
    }

    @CallSuper
    override fun onItemRangeChanged(
        group: Group,
        positionStart: Int,
        itemCount: Int,
        payload: Any?
    ) {
        observable.onItemRangeChanged(
            this,
            getItemCountBeforeGroup(group) + positionStart,
            itemCount,
            payload
        )
    }

    @CallSuper
    override fun onItemRangeInserted(group: Group, positionStart: Int, itemCount: Int) {
        observable.onItemRangeInserted(
            this,
            getItemCountBeforeGroup(group) + positionStart,
            itemCount
        )
    }

    @CallSuper
    override fun onItemRangeRemoved(group: Group, positionStart: Int, itemCount: Int) {
        observable.onItemRangeRemoved(
            this,
            getItemCountBeforeGroup(group) + positionStart,
            itemCount
        )
    }

    @CallSuper
    override fun onItemMoved(group: Group, fromPosition: Int, toPosition: Int) {
        val groupPosition = getItemCountBeforeGroup(group)
        observable.onItemMoved(this, groupPosition + fromPosition, groupPosition + toPosition)
    }

    /**
     * A group should use this to notify that there is a change in itself.
     *
     * @param positionStart
     * @param itemCount
     */
    @CallSuper
    fun notifyItemRangeInserted(positionStart: Int, itemCount: Int) {
        observable.onItemRangeInserted(this, positionStart, itemCount)
    }

    @CallSuper
    fun notifyItemRangeRemoved(positionStart: Int, itemCount: Int) {
        observable.onItemRangeRemoved(this, positionStart, itemCount)
    }

    @CallSuper
    fun notifyItemMoved(fromPosition: Int, toPosition: Int) {
        observable.onItemMoved(this, fromPosition, toPosition)
    }

    @CallSuper
    fun notifyChanged() {
        observable.onChanged(this)
    }

    @CallSuper
    fun notifyItemInserted(position: Int) {
        observable.onItemInserted(this, position)
    }

    @CallSuper
    fun notifyItemChanged(position: Int) {
        observable.onItemChanged(this, position)
    }

    @CallSuper
    fun notifyItemChanged(position: Int, payload: Any) {
        observable.onItemChanged(this, position, payload)
    }

    @CallSuper
    fun notifyItemRemoved(position: Int) {
        observable.onItemRemoved(this, position)
    }

    @CallSuper
    fun notifyItemRangeChanged(positionStart: Int, itemCount: Int) {
        observable.onItemRangeChanged(this, positionStart, itemCount)
    }

    @CallSuper
    fun notifyItemRangeChanged(positionStart: Int, itemCount: Int, payload: Any?) {
        observable.onItemRangeChanged(this, positionStart, itemCount, payload)
    }

    /**
     * Iterate in reverse order in case any observer decides to remove themself from the list
     * in their callback
     */
    private class GroupDataObservable {

        internal val observers: MutableList<GroupDataObserver> = ArrayList()

        internal fun onItemRangeChanged(group: Group, positionStart: Int, itemCount: Int) {
            observers.reversed().forEach {
                it.onItemRangeChanged(group, positionStart, itemCount)
            }
        }

        internal fun onItemRangeChanged(
            group: Group,
            positionStart: Int,
            itemCount: Int,
            payload: Any?
        ) {
            observers.reversed().forEach {
                it.onItemRangeChanged(group, positionStart, itemCount, payload)
            }
        }

        internal fun onItemInserted(group: Group, position: Int) {
            observers.reversed().forEach {
                it.onItemInserted(group, position)
            }
        }

        internal fun onItemChanged(group: Group, position: Int) {
            observers.reversed().forEach {
                it.onItemChanged(group, position)
            }
        }

        internal fun onItemChanged(group: Group, position: Int, payload: Any?) {
            observers.reversed().forEach {
                it.onItemChanged(group, position, payload)
            }
        }

        internal fun onItemRemoved(group: Group, position: Int) {
            observers.reversed().forEach {
                it.onItemRemoved(group, position)
            }
        }

        internal fun onItemRangeInserted(group: Group, positionStart: Int, itemCount: Int) {
            for (i in observers.indices.reversed()) {
                observers[i].onItemRangeInserted(group, positionStart, itemCount)
            }
        }

        internal fun onItemRangeRemoved(group: Group, positionStart: Int, itemCount: Int) {
            observers.reversed().forEach {
                it.onItemRangeRemoved(group, positionStart, itemCount)
            }
        }

        internal fun onItemMoved(group: Group, fromPosition: Int, toPosition: Int) {
            observers.reversed().forEach {
                it.onItemMoved(group, fromPosition, toPosition)
            }
        }

        internal fun onChanged(group: Group) {
            observers.reversed().forEach {
                it.onChanged(group)
            }
        }

        internal fun registerObserver(observer: GroupDataObserver) {
            synchronized(observers) {
                if (observers.contains(observer)) {
                    throw IllegalStateException("Observer $observer is already registered.")
                }
                observers.add(observer)
            }
        }

        internal fun unregisterObserver(observer: GroupDataObserver) {
            synchronized(observers) {
                val index = observers.indexOf(observer)
                observers.removeAt(index)
            }
        }
    }
}