package soulless.com.transitiontest.list

import android.os.AsyncTask
import android.support.v7.util.DiffUtil
import soulless.com.transitiontest.util.logv
import java.lang.ref.WeakReference
import kotlin.system.measureTimeMillis

internal class DiffTask(
    asyncDiffUtil: AsyncDiffUtil,
    private val diffCallback: DiffUtil.Callback,
    private val runGeneration: Int
) : AsyncTask<Void, Void, DiffUtil.DiffResult>() {

    private val asyncListDiffer: WeakReference<AsyncDiffUtil> = WeakReference(asyncDiffUtil)

    override fun doInBackground(vararg voids: Void): DiffUtil.DiffResult {
        var result: DiffUtil.DiffResult? = null
        val elapsed = measureTimeMillis {
            logv("diffUtil started")
            result =  DiffUtil.calculateDiff(diffCallback)
            logv("diffUtil finished")
        }
        logv("async diffUtil took $elapsed")
        return result!!
    }

    override fun onPostExecute(diffResult: DiffUtil.DiffResult) {
        logv("diffUtil postExecute")
        val async = asyncListDiffer.get()
        val groups = async?.groups
        if (async != null && groups != null && runGeneration == async.maxScheduledGeneration) {
            logv("diffUtil postExecute correct")
            async.asyncDiffUtilCallback.onDispatchResult(groups)
            diffResult.dispatchUpdatesTo(async.asyncDiffUtilCallback)
            logv("diffUtil postExecute dispatched")
        }
        logv("diffUtil postExecute finished")
    }
}