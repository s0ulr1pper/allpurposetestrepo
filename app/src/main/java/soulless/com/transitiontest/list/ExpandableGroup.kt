package soulless.com.transitiontest.list

import java.util.*

/**
 * An ExpandableContentItem is one "base" content item with a list of children (any of which
 * may themselves be a group.)
 */

class ExpandableGroup : NestedGroup {

    private val onExpandedListeners = ArrayList<(Boolean) -> Unit>()

    var isExpandable = true

    var isExpanded: Boolean
        private set
    private val parent: ExpandableItem
    private val children = ArrayList<Group>()

    constructor(expandableItem: ExpandableItem) : this(expandableItem, false)

    constructor(expandableItem: ExpandableItem, isExpanded: Boolean) {
        this.parent = expandableItem
        expandableItem.registerGroupDataObserver(this)
        expandableItem.setExpandableGroup(this)
        this.isExpanded = isExpanded
    }

    override fun add(group: Group) {
        super.add(group)
        if (isExpanded) {
            val itemCount = getItemCount()
            children.add(group)
            notifyItemRangeInserted(itemCount, group.getItemCount())
        } else {
            children.add(group)
        }
    }

    override fun addAll(groups: Collection<Group>) {
        if (groups.isEmpty()) return
        super.addAll(groups)
        if (isExpanded) {
            val itemCount = getItemCount()
            this.children.addAll(groups)
            notifyItemRangeInserted(itemCount, getItemCount(groups))
        } else {
            this.children.addAll(groups)
        }
    }

    override fun addAll(position: Int, groups: Collection<Group>) {
        if (groups.isEmpty()) {
            return
        }
        super.addAll(position, groups)
        if (isExpanded) {
            val itemCount = getItemCount()
            this.children.addAll(position, groups)
            notifyItemRangeInserted(itemCount, getItemCount(groups))
        } else {
            this.children.addAll(position, groups)
        }
    }

    override fun getGroup(position: Int): Group {
        return if (position == 0) {
            parent
        } else {
            children[position - 1]
        }
    }

    fun getPosition(group: Group): Int {
        return if (group === parent) {
            0
        } else {
            1 + children.indexOf(group)
        }
    }

    override fun getGroupCount(): Int {
        return 1 + if (isExpanded) children.size else 0
    }

    fun addOnExpandedListener(listener: (Boolean) -> Unit) {
        onExpandedListeners.add(listener)
    }

    fun removeOnExpandedListener(listener: (Boolean) -> Unit) {
        onExpandedListeners.remove(listener)
    }

    fun expand() {
        if (!isExpanded) toggle()
    }

    fun collapse() {
        if (isExpanded) toggle()
    }

    fun toggle() {
        if (!isExpandable) return

        val oldSize = getItemCount()
        isExpanded = !isExpanded
        val newSize = getItemCount()
        if (oldSize > newSize) {
            notifyItemRangeRemoved(newSize, oldSize - newSize)
        } else {
            notifyItemRangeInserted(oldSize, newSize - oldSize)
        }

        onExpandedListeners.forEach { it.invoke(isExpanded) }
    }

    private fun dispatchChildChanges(group: Group): Boolean {
        return isExpanded || group === parent
    }

    override fun onChanged(group: Group) {
        if (dispatchChildChanges(group)) {
            super.onChanged(group)
        }
    }

    override fun onItemInserted(group: Group, position: Int) {
        if (dispatchChildChanges(group)) {
            super.onItemInserted(group, position)
        }
    }

    override fun onItemChanged(group: Group, position: Int) {
        if (dispatchChildChanges(group)) {
            super.onItemChanged(group, position)
        }
    }

    override fun onItemChanged(group: Group, position: Int, payload: Any?) {
        if (dispatchChildChanges(group)) {
            super.onItemChanged(group, position, payload)
        }
    }

    override fun onItemRemoved(group: Group, position: Int) {
        if (dispatchChildChanges(group)) {
            super.onItemRemoved(group, position)
        }
    }

    override fun onItemRangeChanged(group: Group, positionStart: Int, itemCount: Int) {
        if (dispatchChildChanges(group)) {
            super.onItemRangeChanged(group, positionStart, itemCount)
        }
    }

    override fun onItemRangeChanged(
        group: Group,
        positionStart: Int,
        itemCount: Int,
        payload: Any?
    ) {
        if (dispatchChildChanges(group)) {
            super.onItemRangeChanged(group, positionStart, itemCount, payload)
        }
    }

    override fun onItemRangeInserted(group: Group, positionStart: Int, itemCount: Int) {
        if (dispatchChildChanges(group)) {
            super.onItemRangeInserted(group, positionStart, itemCount)
        }
    }

    override fun onItemRangeRemoved(group: Group, positionStart: Int, itemCount: Int) {
        if (dispatchChildChanges(group)) {
            super.onItemRangeRemoved(group, positionStart, itemCount)
        }
    }

    override fun onItemMoved(group: Group, fromPosition: Int, toPosition: Int) {
        if (dispatchChildChanges(group)) {
            super.onItemMoved(group, fromPosition, toPosition)
        }
    }
}
