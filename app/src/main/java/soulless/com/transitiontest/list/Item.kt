package soulless.com.transitiontest.list

import android.support.v7.widget.RecyclerView
import android.view.View
import java.util.concurrent.atomic.AtomicLong

/**
 *
 * @author novikov
 *         Date: 22.11.2017
 */
abstract class Item<VH : ViewHolder> : Group {

    private var parentDataObserver: GroupDataObserver? = null

    /**
     * If you don't specify an id, this id is an auto-generated unique negative integer for each Item (the less
     * likely to conflict with your model IDs.)
     * <p>
     * You may prefer to override it with the ID of a model object, for example the primary key of
     * an object from a database that it represents.
     *
     * @return A unique id
     */
    open val id = ID_COUNTER.decrementAndGet()

    final override fun getItemCount() = 1

    abstract fun getSpanSize(defaultSpanSize: Int): Int

    /** Default implementations is [id], override if item is identified by type different from [Long] */
    open fun identifySelf(): Any = id

    protected abstract fun bind(viewHolder: VH)

    /**
     * If you don't specify how to handle payloads in your implementation, they'll be ignored and
     * the adapter will do a full rebind.
     *
     * @param holder The ViewHolder to bind
     * @param position The adapter position
     * @param payloads A list of payloads (may be empty)
     */
    protected open fun bind(viewHolder: VH, payloads: List<Any>) {
        bind(viewHolder)
    }

    open fun recycle(viewHolder: VH) {
    }

    /**
     * This function can be called when an item needs to be prepared to be displayed
     * For example an image to to be fetched
     */
    open fun prepareItem(viewHolder: VH) {
    }

    abstract fun getLayout(): Int

    abstract fun createViewHolder(itemView: View): VH

    @Suppress("UNCHECKED_CAST")
    fun bindViewHolder(holder: VH, position: Int, payloads: List<Any>) {
        holder.bindItem(this as Item<ViewHolder>)
        bind(holder, payloads)
    }

    @Suppress("UNCHECKED_CAST")
    override fun getItem(position: Int): Item<ViewHolder> {
        return if (position == 0) {
            this as Item<ViewHolder>
        } else {
            throw RuntimeException("incorrect position")
        }
    }

    override fun getItemPosition(item: Group) = if (this === item) 0 else RecyclerView.NO_POSITION

    override fun registerGroupDataObserver(groupDataObserver: GroupDataObserver) {
        this.parentDataObserver = groupDataObserver
    }

    override fun unregisterGroupDataObserver(groupDataObserver: GroupDataObserver) {
        parentDataObserver = null
    }

    fun notifyChanged() {
        parentDataObserver?.onItemChanged(this, 0)
    }

    fun notifyChanged(payload: Any) {
        parentDataObserver?.onItemChanged(this, 0, payload)
    }

    /**
     * Whether two item objects represent the same underlying data when compared using DiffUtil,
     * even if there has been a change in that data.
     *
     * The default implementation compares both view type and id.
     */
    fun isSameAs(other: Item<*>): Boolean {
        return when {
            getLayout() != other.getLayout() -> false
            else -> identifySelf() == other.identifySelf()
        }
    }

    open fun getChangePayload(newItem: Item<*>): Any? {
        return null
    }

    companion object {

        private val ID_COUNTER = AtomicLong(0)
    }
}