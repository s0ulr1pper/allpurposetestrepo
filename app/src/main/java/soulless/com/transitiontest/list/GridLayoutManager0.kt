package soulless.com.transitiontest.list

import android.content.Context
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.View

/**
 *
 * @author novikov
 *         Date: 07.06.2018
 */
class GridLayoutManager0 : GridLayoutManager {
    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes)

    constructor(
        context: Context?, spanCount: Int
    ) : super(context, spanCount)

    constructor(
        context: Context?,
        spanCount: Int,
        orientation: Int,
        reverseLayout: Boolean
    ) : super(context, spanCount, orientation, reverseLayout)

    override fun onFocusSearchFailed(
        focused: View?,
        focusDirection: Int,
        recycler: RecyclerView.Recycler?,
        state: RecyclerView.State?
    ): View? {
        return null
    }
}