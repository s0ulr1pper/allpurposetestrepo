package soulless.com.transitiontest.list

/**
 *
 * @author novikov
 *         Date: 22.11.2017
 */
interface Group {

    fun getItemCount(): Int

    fun getItemPosition(item: Group): Int

    /**
     * Returns item by its [position]
     * Implementing this interface usually required cast, as long as generics are invariant
     * but in our case every item generic has [ViewHolder] as lower bound
     */
    fun getItem(position: Int): Item<ViewHolder>

    fun order() = -1

    fun registerGroupDataObserver(groupDataObserver: GroupDataObserver)

    fun unregisterGroupDataObserver(groupDataObserver: GroupDataObserver)
}