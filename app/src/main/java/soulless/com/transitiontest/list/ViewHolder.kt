package soulless.com.transitiontest.list

import android.support.v7.widget.RecyclerView
import android.view.View
import java.util.*

/**
 *
 * @author novikov
 *         Date: 22.11.2017
 */
abstract class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val extras: Map<String, Any> = HashMap()

    lateinit var item: Item<in ViewHolder>
        private set

    fun bindItem(item: Item<ViewHolder>) {
        this.item = item
    }
}

class EmptyViewHolder(itemView: View) : ViewHolder(itemView)