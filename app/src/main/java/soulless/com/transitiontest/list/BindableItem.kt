package soulless.com.transitiontest.list

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.view.View

/**
 *
 * @author novikov
 *         Date: 22.11.2017
 */
abstract class BindableItem<T : ViewDataBinding> : Item<BindingViewHolder<T>>() {

    override fun createViewHolder(itemView: View): BindingViewHolder<T> {
        val binding = DataBindingUtil.bind<T>(itemView) ?: throw RuntimeException()
        return BindingViewHolder(binding)
    }
}