package soulless.com.transitiontest.list

/**
 *
 * @author novikov
 *         Date: 28.12.2017
 */
interface Identifiable {

    fun identifySelf(): Any
}