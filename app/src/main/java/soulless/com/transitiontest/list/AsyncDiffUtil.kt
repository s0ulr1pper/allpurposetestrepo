package soulless.com.transitiontest.list

import android.support.annotation.MainThread
import android.support.v7.util.DiffUtil
import android.support.v7.util.ListUpdateCallback

internal class AsyncDiffUtil(val asyncDiffUtilCallback: Callback) {
    var maxScheduledGeneration: Int = 0
        private set
    var groups: Collection<Group>? = null
        private set

    internal interface Callback : ListUpdateCallback {
        /**
         * Called on the main thread before DiffUtil dispatches the result
         */
        @MainThread
        fun onDispatchResult(newGroups: Collection<Group>)
    }

    fun calculateDiff(newGroups: Collection<Group>, diffUtilCallback: DiffUtil.Callback) {
        groups = newGroups
        // incrementing generation means any currently-running diffs are discarded when they finish
        val runGeneration = ++maxScheduledGeneration
        DiffTask(this, diffUtilCallback, runGeneration).execute()
    }
}
