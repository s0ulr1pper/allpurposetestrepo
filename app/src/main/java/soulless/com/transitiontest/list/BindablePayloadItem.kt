package soulless.com.transitiontest.list

import android.databinding.ViewDataBinding

/**
 *
 * @author novikov
 *         Date: 22.11.2017
 */
abstract class BindablePayloadItem<T : ViewDataBinding, out P> : BindableItem<T>()