package soulless.com.transitiontest.list

import android.databinding.ViewDataBinding

/**
 *
 * @author novikov
 *         Date: 22.11.2017
 */
class BindingViewHolder<out T : ViewDataBinding>(val binding: T) : ViewHolder(binding.root)