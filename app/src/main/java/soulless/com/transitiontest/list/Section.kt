package soulless.com.transitiontest.list

import android.support.v7.util.DiffUtil
import android.support.v7.util.ListUpdateCallback

/**
 *
 * @author novikov
 *         Date: 23.11.2017
 */
open class Section(
    private var header: Group? = null,
    content: List<Group>? = null
) : NestedGroup() {

    private val children = ArrayList<Group>()

    init {
        content?.let {
            addAll(it)
        }
    }

    private var placeholder: Group? = null

    private var footer: Group? = null

    private var hideWhenEmpty = false

    private var isHeaderAndFooterVisible = true

    private var isPlaceholderVisible = false

    override fun getGroup(position: Int): Group {
        var positionInner = position
        if (isHeaderShown() && positionInner == 0) {
            return header!!
        }
        positionInner -= getHeaderCount()
        if (isPlaceholderShown() && positionInner == 0) {
            return placeholder!!
        }
        positionInner -= getPlaceholderCount()
        return if (positionInner == children.size) {
            if (isFooterShown()) {
                footer!!
            } else {
                throw IndexOutOfBoundsException(
                    "Wanted group at position " + position +
                            " but there are only " + getGroupCount() + " groups"
                )
            }
        } else {
            children[positionInner]
        }
    }

    override fun getGroupCount() =
        getHeaderCount() + getFooterCount() + getPlaceholderCount() + children.size

    override fun getItemPosition(item: Group): Int {
        var count = 0
        if (isHeaderShown()) {
            if (item === header) {
                return count
            }
        }
        count += getHeaderCount()
        if (isPlaceholderShown()) {
            if (item === placeholder) {
                return count
            }
        }
        count += getPlaceholderCount()

        val index = children.indexOf(item)
        if (index >= 0) {
            return count + index
        }
        count += children.size

        if (isFooterShown()) {
            if (footer === item) {
                return count
            }
        }

        return -1
    }

    override fun add(position: Int, group: Group) {
        super.add(position, group)
        children.add(position, group)
        val notifyPosition = getHeaderItemCount() + getItemCount(children.subList(0, position))
        notifyItemRangeInserted(notifyPosition, group.getItemCount())
        refreshEmptyState()
    }

    final override fun addAll(groups: Collection<Group>) {
        if (groups.isEmpty()) return
        super.addAll(groups)
        val position = getPositionWithoutHeader()
        this.children.addAll(groups)
        notifyItemRangeInserted(position, getItemCount(groups))
        refreshEmptyState()
    }

    override fun addAll(position: Int, groups: Collection<Group>) {
        if (groups.isEmpty()) {
            return
        }

        super.addAll(position, groups)
        this.children.addAll(position, groups)

        val notifyPosition = getHeaderItemCount() + getItemCount(children.subList(0, position))
        notifyItemRangeInserted(notifyPosition, getItemCount(groups))
        refreshEmptyState()
    }

    override fun add(group: Group) {
        super.add(group)
        val position = getPositionWithoutHeader()
        children.add(group)
        notifyItemRangeInserted(position, group.getItemCount())
        refreshEmptyState()
    }

    override fun remove(group: Group) {
        super.remove(group)
        val position = getItemCountBeforeGroup(group)
        children.remove(group)
        notifyItemRangeRemoved(position, group.getItemCount())
        refreshEmptyState()
    }

    override fun removeAll(groups: Collection<Group>) {
        if (groups.isEmpty()) {
            return
        }

        super.removeAll(groups)
        for (group in groups) {
            val position = getItemCountBeforeGroup(group)
            children.remove(group)
            notifyItemRangeRemoved(position, group.getItemCount())
        }
        refreshEmptyState()
    }

    /**
     * Replace all existing body content and dispatch fine-grained change notifications to the
     * parent using DiffUtil.
     *
     *
     * Item comparisons are made using:
     * - Item.isSameAs(Item otherItem) (are items the same?)
     * - Item.equals() (are contents the same?)
     *
     *
     * If you don't customize getId() or isSameAs() and equals(), the default implementations will return false,
     * meaning your Group will consider every update a complete change of everything.
     *
     * @param newBodyGroups The new content of the section
     * @param forceUpdate forces update whole section content, ignore DiffUtil
     */
    fun update(newBodyGroups: Collection<Group>, forceUpdate: Boolean = false) {

        val oldBodyGroups = ArrayList(children)
        val oldBodyItemCount = getItemCount(oldBodyGroups)
        val newBodyItemCount = getItemCount(newBodyGroups)

        super.removeAll(children)
        children.clear()
        children.addAll(newBodyGroups)
        super.addAll(newBodyGroups)

        if (forceUpdate) {
            val headerItemCount = getHeaderItemCount()
            when {
                newBodyItemCount == 0 -> refreshEmptyState()
                oldBodyItemCount == newBodyItemCount -> notifyItemRangeChanged(
                    headerItemCount,
                    oldBodyItemCount
                )
                oldBodyItemCount < newBodyItemCount -> {
                    notifyItemRangeChanged(headerItemCount, oldBodyItemCount)
                    notifyItemRangeInserted(
                        headerItemCount + oldBodyItemCount,
                        newBodyItemCount - oldBodyItemCount
                    )
                }
                oldBodyItemCount > newBodyItemCount -> {
                    notifyItemRangeChanged(headerItemCount, newBodyItemCount)
                    notifyItemRangeRemoved(
                        headerItemCount + newBodyItemCount,
                        oldBodyItemCount - newBodyItemCount
                    )
                }
            }
            if (newBodyItemCount == 0 || oldBodyItemCount == 0) {
                refreshEmptyState()
            }

            return
        }

        val diffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun getOldListSize(): Int {
                return oldBodyItemCount
            }

            override fun getNewListSize(): Int {
                return newBodyItemCount
            }

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldItem = getItem(oldBodyGroups, oldItemPosition)
                val newItem = getItem(newBodyGroups, newItemPosition)
                return newItem.isSameAs(oldItem)
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldItem = getItem(oldBodyGroups, oldItemPosition)
                val newItem = getItem(newBodyGroups, newItemPosition)
                return newItem == oldItem
            }

            override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
                val oldItem = getItem(oldBodyGroups, oldItemPosition)
                val newItem = getItem(newBodyGroups, newItemPosition)
                return oldItem.getChangePayload(newItem)
            }
        })

        if (newBodyItemCount == 0 || oldBodyItemCount == 0) {
            refreshEmptyState()
        }
        diffResult.dispatchUpdatesTo(listUpdateCallback)
    }

    private val listUpdateCallback: ListUpdateCallback = object : ListUpdateCallback {
        override fun onInserted(position: Int, count: Int) {
            notifyItemRangeInserted(getHeaderItemCount() + position, count)
        }

        override fun onRemoved(position: Int, count: Int) {
            notifyItemRangeRemoved(getHeaderItemCount() + position, count)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            val headerItemCount = getHeaderItemCount()
            notifyItemMoved(headerItemCount + fromPosition, headerItemCount + toPosition)
        }

        override fun onChanged(position: Int, count: Int, payload: Any?) {
            notifyItemRangeChanged(getHeaderItemCount() + position, count, payload)
        }
    }

    private fun getItem(groups: Collection<Group>, position: Int): Item<*> {
        var previousPosition = 0

        for (group in groups) {
            val size = group.getItemCount()
            if (size + previousPosition > position) {
                return group.getItem(position - previousPosition)
            }
            previousPosition += size
        }

        throw IndexOutOfBoundsException(
            "Wanted item at " + position + " but there are only "
                    + previousPosition + " items"
        )
    }

    /**
     * Optional. Set a placeholder for when the section's body is empty.
     *
     *
     * If setHideWhenEmpty(true) is set, then the empty placeholder will not be shown.
     *
     * @param placeholder A placeholder to be shown when there is no body content
     */
    fun setPlaceholder(placeholder: Group?) {

        if (placeholder == null)
            throw NullPointerException("Placeholder can't be null.  Please use removePlaceholder() instead!")
        if (this.placeholder != null) {
            removePlaceholder()
        }
        this.placeholder = placeholder
        refreshEmptyState()
    }

    fun removePlaceholder() {
        hidePlaceholder()
        this.placeholder = null
    }

    private fun showPlaceholder() {
        if (isPlaceholderVisible || placeholder == null) return
        isPlaceholderVisible = true
        notifyItemRangeInserted(getHeaderItemCount(), placeholder!!.getItemCount())
    }

    private fun hidePlaceholder() {
        if (!isPlaceholderVisible || placeholder == null) return
        isPlaceholderVisible = false
        notifyItemRangeRemoved(getHeaderItemCount(), placeholder!!.getItemCount())
    }

    /**
     * Whether a section's contents are visually empty
     * @return
     */
    fun isEmpty(): Boolean {
        return children.isEmpty() || getItemCount(children) == 0
    }

    fun clear() {
        super.removeAll(children)
        children.clear()
        notifyChanged()
        refreshEmptyState()
    }

    private fun hideDecorations() {
        if (!isHeaderAndFooterVisible && !isPlaceholderVisible) return
        val count = getHeaderItemCount() + getPlaceholderItemCount() + getFooterItemCount()
        isHeaderAndFooterVisible = false
        isPlaceholderVisible = false
        notifyItemRangeRemoved(0, count)
    }

    protected fun refreshEmptyState() {
        val isEmpty = isEmpty()
        if (isEmpty) {
            if (hideWhenEmpty) {
                hideDecorations()
            } else {
                showPlaceholder()
                showHeadersAndFooters()
            }
        } else {
            hidePlaceholder()
            showHeadersAndFooters()
        }
    }

    private fun showHeadersAndFooters() {
        if (isHeaderAndFooterVisible) {
            return
        }
        isHeaderAndFooterVisible = true
        notifyItemRangeInserted(0, getHeaderItemCount())
        notifyItemRangeInserted(getItemCount(), getFooterItemCount())
    }

    private fun getBodyItemCount(): Int {
        return if (isPlaceholderVisible) getPlaceholderItemCount() else getItemCount(children)
    }

    private fun getPositionWithoutHeader(): Int {
        return getBodyItemCount() + getHeaderItemCount()
    }

    private fun getHeaderCount(): Int {
        return if (header == null || !isHeaderAndFooterVisible) 0 else 1
    }

    private fun getHeaderItemCount(): Int {
        return if (getHeaderCount() == 0) 0 else header!!.getItemCount()
    }

    private fun getFooterItemCount(): Int {
        return if (getFooterCount() == 0) 0 else footer!!.getItemCount()
    }

    private fun getFooterCount(): Int {
        return if (footer == null || !isHeaderAndFooterVisible) 0 else 1
    }

    private fun getPlaceholderCount(): Int {
        return if (isPlaceholderVisible) 1 else 0
    }

    fun getPosition(group: Group): Int {
        var count = 0
        if (isHeaderShown()) {
            if (group === header) return count
        }
        count += getHeaderCount()
        if (isPlaceholderShown()) {
            if (group === placeholder) return count
        }
        count += getPlaceholderCount()

        val index = children.indexOf(group)
        if (index >= 0) return count + index
        count += children.size

        if (isFooterShown()) {
            if (footer === group) {
                return count
            }
        }

        return -1
    }

    private fun isHeaderShown(): Boolean {
        return getHeaderCount() > 0
    }

    private fun isFooterShown(): Boolean {
        return getFooterCount() > 0
    }

    private fun isPlaceholderShown(): Boolean {
        return getPlaceholderCount() > 0
    }

    fun getHeader() = header

    fun setHeader(header: Group?) {
        if (header == null)
            throw NullPointerException("Header can't be null.  Please use removeHeader() instead!")
        val previousHeaderItemCount = getHeaderItemCount()
        this.header = header
        notifyHeaderItemsChanged(previousHeaderItemCount)
    }

    fun removeHeader() {
        val previousHeaderItemCount = getHeaderItemCount()
        this.header = null
        notifyHeaderItemsChanged(previousHeaderItemCount)
    }

    private fun notifyHeaderItemsChanged(previousHeaderItemCount: Int) {
        val newHeaderItemCount = getHeaderItemCount()
        when {
            newHeaderItemCount == previousHeaderItemCount -> notifyItemRangeChanged(
                0,
                newHeaderItemCount
            )
            previousHeaderItemCount > 0 -> notifyItemRangeRemoved(0, previousHeaderItemCount)
            newHeaderItemCount > 0 -> notifyItemRangeInserted(0, newHeaderItemCount)
        }
    }

    fun setFooter(footer: Group?) {
        if (footer == null)
            throw NullPointerException("Footer can't be null.  Please use removeFooter() instead!")
        val previousFooterItemCount = getFooterItemCount()
        this.footer = footer
        notifyFooterItemsChanged(previousFooterItemCount)
    }

    fun removeFooter() {
        val previousFooterItemCount = getFooterItemCount()
        this.footer = null
        notifyFooterItemsChanged(previousFooterItemCount)
    }

    private fun notifyFooterItemsChanged(previousFooterItemCount: Int) {
        val newFooterItemCount = getFooterItemCount()
        if (previousFooterItemCount > 0) {
            notifyItemRangeRemoved(getPositionWithoutHeader(), previousFooterItemCount)
        }
        if (newFooterItemCount > 0) {
            notifyItemRangeInserted(getPositionWithoutHeader(), newFooterItemCount)
        }
    }

    fun setHideWhenEmpty(hide: Boolean) {
        if (hideWhenEmpty == hide) return
        hideWhenEmpty = hide
        refreshEmptyState()
    }

    override fun onItemInserted(group: Group, position: Int) {
        super.onItemInserted(group, position)
        refreshEmptyState()
    }

    override fun onItemRemoved(group: Group, position: Int) {
        super.onItemRemoved(group, position)
        refreshEmptyState()
    }

    override fun onItemRangeInserted(group: Group, positionStart: Int, itemCount: Int) {
        super.onItemRangeInserted(group, positionStart, itemCount)
        refreshEmptyState()
    }

    override fun onItemRangeRemoved(group: Group, positionStart: Int, itemCount: Int) {
        super.onItemRangeRemoved(group, positionStart, itemCount)
        refreshEmptyState()
    }

    private fun getPlaceholderItemCount(): Int {
        return if (isPlaceholderVisible && placeholder != null) {
            placeholder!!.getItemCount()
        } else 0
    }
}