package soulless.com.transitiontest

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import soulless.com.transitiontest.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val binding by lazy(LazyThreadSafetyMode.NONE) {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    val items = 15

    val adapter = TestAdapter(0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.leftButton.setOnClickListener {
            test0()
        }

        binding.rightButton.setOnClickListener {
            test()
        }
    }

    fun test0() {
        supportFragmentManager.beginTransaction().also {
            it.setCustomAnimations(
                R.anim.slide_in_top,
                R.anim.slide_out_bottom,
                R.anim.slide_in_top,
                R.anim.slide_out_bottom
            )
            it.addToBackStack("0" + BlankFragment::class.java.canonicalName)
        }.add(R.id.fragment_placeholder, BlankFragment()).commit()
    }

    fun test() {
        supportFragmentManager.beginTransaction().also {
            it.setCustomAnimations(
                R.anim.slide_in_top,
                R.anim.slide_out_bottom,
                R.anim.slide_in_top,
                R.anim.slide_out_bottom
            )
            it.addToBackStack("1" + BlankRootFragment::class.java.canonicalName)
        }.add(R.id.fragment_placeholder, BlankRootFragment()).commit()
    }

}
