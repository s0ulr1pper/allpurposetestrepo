package soulless.com.transitiontest

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.*

class BlankFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank, container, false).also {
            val rnd = Random()
            it.findViewById<View>(R.id.test_root).setBackgroundColor(
                Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
            )
        }
    }

}
