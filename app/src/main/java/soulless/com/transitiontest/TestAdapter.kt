package soulless.com.transitiontest

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class TestAdapter(items: Int) : RecyclerView.Adapter<TestViewHolder>() {

    val content = MutableList(items, ::TestModel)

    fun addModel(items: Int = 1) {
        (0 until items).forEach {
            content.add(0, TestModel(content.size))
        }
        notifyItemRangeInserted(0, items)
    }

    fun removeModel(items: Int = 1) {
        if (content.size < items) {
            return
        }
        (0 until items).forEach {
            content.removeAt(0)
        }
        notifyItemRangeRemoved(0, items)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestViewHolder {
        return TestViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_test, parent, false))
    }

    override fun getItemCount() = content.size

    override fun onBindViewHolder(holder: TestViewHolder, position: Int) {
        holder.testTv.text = content[position].number.toString()
    }
}

class TestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val testTv = itemView.findViewById<TextView>(R.id.test_id)

}

class TestModel(val number: Int)