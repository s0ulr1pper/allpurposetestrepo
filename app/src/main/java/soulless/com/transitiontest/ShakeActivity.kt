package soulless.com.transitiontest

import android.hardware.SensorManager
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import soulless.com.transitiontest.test.ShakeDetector

/**
 *
 * @author novikov
 *         Date: 29.08.2018
 */
class ShakeActivity : AppCompatActivity(), ShakeDetector.Listener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val shaker = ShakeDetector(this)
        shaker.start(getSystemService(SENSOR_SERVICE) as SensorManager)
    }

    override fun hearShake() {
        Log.d("XXX", "SHAKE IT BABY")
    }
}